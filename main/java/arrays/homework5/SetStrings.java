/*
 * Copyright (c) 2020. by LYNUX (Cristian F.)
 */
//  Set with strings:
//        1. Create a HashSet set1 with strings and add the following values: Green, Red, Blue.
//        2. Print the set.
//        3. Add values Yellow, Green to the set.
//        4. Print the set. Why does it contain only one value of Green?
//        5. Print the size of the set.
//        6. Check if value Blue is contained in the list and print the result.
//        7. Create another HashSet set2 with strings and add the following values: White, Black.
//        8. Check if set1 contains set2 - use method containsAll(). Print the result.
//        9. Add set2 to set1 - use method addAll().
//        10. Print set1.
//        11. Remove value Red from set1. Print the set1.
//        12. Iterate over set1 and print each value on a new line. Use Method2 from  https://www.geeksforgeeks.org/iterating-arraylists-java/.
//        13. What if instead of HashSet in above exercises we use LinkedHashSet? Is it still going to work the same?

package arrays.homework5;

import java.util.*;

public class SetStrings {
    public static void main(String[] args) {
        HashSet<Object> set1 = new HashSet<>(); // 1. Create a HasSet set1 with strings and add the following values: Green, Red, Blue.
        set1.add("Green");
        set1.add("Red");
        set1.add("Blue");
        System.out.println(set1); // 2. Print the set.

        set1.add("Yellow"); // 3. Add values Yellow, Green to the set.
        set1.add("Green");
        System.out.println("1-st Set list: " + set1); // 4. Print the list. Why does it contain only one value of Green?
        int SetSize = set1.size();                  // answer: because the green color is already in the old list, and is ignored.
        System.out.println("Size of the set1 = " + SetSize); // 5. Print the size of the set.

        // 6. Check if value Blue is contained in the list and print the result.
        System.out.println("Is >>> " + set1.contains("Blue") + " <<< and found Blue in the list!");

        // 7. Create another HasSet set2 with strings and add the following values: White, Black.
        HashSet<Object> set2 = new HashSet<>();
        set2.add("White");
        set2.add("Black");
        System.out.println("Colors of the List 2 are: " + set2);

        // 8. Check if set1 contains list2 - use method containsAll(). Print the result.
        System.out.println("White and Black Colors aren't in the 1-st list so, is >>> " + set1.containsAll(set2) + " <<< !");

        // 9. Add list2 to list1 - use method addAll().
        System.out.println("All colors are: " + set2 + set1 + ", are added and => " + set2.addAll(set1));

        // 10. Print set1.
        System.out.println(set1);

        // 11. Remove value Red from list1. Print the list1.
        set1.remove("Red");
        System.out.println(set1 + " => removed the Red color, from the list.");

        // 12. Iterate over list1 and print each value on a new line. Use Method2 from  https://www.geeksforgeeks.org/iterating-arraylists-java/.
        Iterator value = set1.iterator();
        System.out.println("The iterator colors are: ");
        while (value.hasNext()) {
            System.out.println(" - " + value.next());

            // 13. What if instead of ArrayList in above exercises we use LinkedList? Is it still going to work the same?
            // Answer: LinkedList implements it with a doubly-linked list. ArrayList implements it with a dynamically re-sizing array.
            // Explain: In LinkedList the Iterator remove is O(1), meanwhile in ArrayList O(n).
        }
    }
}
