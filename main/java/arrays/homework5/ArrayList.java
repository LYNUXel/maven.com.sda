/*
 * Copyright (c) 2020. by LYNUX (Cristian F.)
 *   >>> Lists with strings <<<
 */
package arrays.homework5;

import java.util.*;

public class ArrayList {
    public static void main(String[] args) {
        List<String> listArray1 = new LinkedList<>(); // 1. Create an ArrayList list1 with strings and add the following values: Green, Red, Blue.
        listArray1.add("Green");
        listArray1.add("Red");
        listArray1.add("Blue");
        System.out.println(listArray1);

        listArray1.add(0, "Yellow"); // 2. Add value Yellow on the first position of the list.
        System.out.println("Arrays list: " + listArray1); // 3. Print the list.
        int ArrSize = listArray1.size();
        System.out.println("Size of the arrays = " + ArrSize); // 4. Print the size of the list.

        Collections.sort(listArray1); // 5. Sort the list using Collections.sort() method.
        System.out.println("Sorted Arrays list: " + listArray1); // 6. Print the list.

        // 7. Check if value Blue is contained in the list and print the result.
        System.out.println("Is >>> " + listArray1.contains("Blue") + " <<< and found Blue in the list!");

        // 8. Create another ArrayList list2 with strings and add the following values: White, Black.
        List<String> listArray2 = new LinkedList<>();
        listArray2.add("White");
        listArray2.add("Black");
        System.out.println("Colors of the List 2 are: " + listArray2);

        // 9. Check if list1 contains list2 - use method containsAll(). Print the result.
        System.out.println("White and Black Colors aren't in the 1-st list so, is >>> " + listArray1.containsAll(listArray2) + " <<< !");

        // 10. Add list2 to list1 - use method addAll().
        System.out.println("All colors are: " + listArray1 + listArray2 + ", are added and => " + listArray1.addAll(listArray2));


        // 12. Remove value Red from list1. Print the list1.
        listArray1.remove("Red");
        System.out.println(listArray1 + " => removed the Red color.");

        // 13. Iterate over list1 and print each value on a new line. Use Method2 from  https://www.geeksforgeeks.org/iterating-arraylists-java/.
        List<String> listArray = Arrays.asList("White", "Red", "Blue");
        Iterator<String> a = listArray.iterator();
        while (a.hasNext())
            System.out.print(a.next() + " ");

        // 14. What if instead of ArrayList in above exercises we use LinkedList? Is it still going to work the same?
        // Answer: LinkedList implements it with a doubly-linked list. ArrayList implements it with a dynamically re-sizing array.
        // Explain: In LinkedList the Iterator remove is O(1), meanwhile in ArrayList O(n).
    }
}